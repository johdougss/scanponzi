<?php

namespace App\Http\Controllers;

use App\Service\ToHelpFast\FinanceiroService;
use Illuminate\Cache\CacheManager;
use Illuminate\Routing\Controller as BaseController;

class HomeController extends BaseController
{

    public function show()
    {
        /** @var CacheManager $cache */
        $cache = app('cache');
        $items = $cache->rememberForever('xx', function () {
            /** @var FinanceiroService $financeiroService */
            $financeiroService = app(FinanceiroService::class);
            $items = $financeiroService->financeiroGeral();
            return $items;
        });

        return view('home', $items);
    }
}
