<?php

namespace App\Service\ToHelpFast;

use App\Service\ToHelpFast\Exceptions\InvalidTokenException;
use App\Service\ToHelpFast\Repositories\AuthenticateRepository;
use App\Service\ToHelpFast\Repositories\DoacoesRecebidasRepository;
use Illuminate\Support\Collection;

class DoacaoService
{

    /**
     * @return mixed
     * @throws InvalidTokenException
     */
    public function get()
    {
        /** @var AuthenticateRepository $authenticateRepository */
        $authenticateRepository = app(AuthenticateRepository::class);
        $authenticates = $authenticateRepository->get();

        /** @var DoacoesRecebidasRepository $doacoesRecebidasRepository */
        $doacoesRecebidasRepository = app(DoacoesRecebidasRepository::class);

        $results = [];
        foreach ($authenticates as $authenticate) {
            /**
             * recebidas
             */
            $doacoesRecebidasRepository->setAuthenticate($authenticate);
            $doacoesRecebidas = $doacoesRecebidasRepository->get();

            /** @var Collection $doacoes */
            $doacoes = $doacoesRecebidas->doar->sortBy(function ($item) {
                return $item->date;
            });

            $doacao = $doacoes->last();

            $results[] = [
                'authenticate' => $authenticate,
                'status_id' => $this->getStatus($doacao),
                'doacao' => $doacao,
            ];
        }

        return $results;
    }


    private function getStatus($doacao): int
    {
        if ($doacao->status === false) {
            return 1;
        }

        if ($doacao->qtd === 0) {
            return 2;
        }

        return 3;
        //vsprintf('doador encontrado [%s/%s] - verificar comprovante', [$doacao->qtd_confirmado, $doacao->qtd]);
    }

}


//doador encontrado - pagamento nao realizado
//{
//    "id": 10680,
//            "valor_total": "28.00",
//            "valor": "0.00",
//            "status": true,
//            "qtd": 1,
//            "qtd_confirmado": 0,
//            "data": "2019-09-03",
//            "hora": "02:48"
//        }
//

//doador encontrado - usuario enviou o comprovante ( nao fez automatico)
//{
//    "id": 10680,
//            "valor_total": "28.00",
//            "valor": "0.00",
//            "status": true,
//            "qtd": 1,
//            "qtd_confirmado": 0,
//            "data": "2019-09-03",
//            "hora": "02:48"
//        }


//doador encontrado e confirmado automatico
//{
//    "id": 10704,
//            "valor_total": "28.00",
//            "valor": "0.00",
//            "status": false,
//            "qtd": 1,
//            "qtd_confirmado": 1,
//            "data": "2019-09-03",
//            "hora": "03:07"
//        }


//doador nao encontrado
//{
//    "id": 11334,
//            "valor_total": "27.50",
//            "valor": "27.50",
//            "status": true,
//            "qtd": 0,
//            "qtd_confirmado": 0,
//            "data": "2019-09-03",
//            "hora": "17:10"
//        }
