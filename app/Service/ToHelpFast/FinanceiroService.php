<?php

namespace App\Service\ToHelpFast;

use App\Service\ToHelpFast\Exceptions\InvalidTokenException;
use App\Service\ToHelpFast\Repositories\AuthenticateRepository;
use App\Service\ToHelpFast\Repositories\DoacoesEnviadasRepository;
use App\Service\ToHelpFast\Repositories\DoacoesRecebidasRepository;
use Exception;
use Illuminate\Support\Arr;

class FinanceiroService
{

    /**
     * @return mixed
     * @throws InvalidTokenException
     * @throws Exception
     */
    public function financeiroGeral()
    {
        /** @var AuthenticateRepository $authenticateRepository */
        $authenticateRepository = app(AuthenticateRepository::class);
        $authenticates = $authenticateRepository->get();

        $items = [];
        foreach ($authenticates as $authenticate) {
            $items[] = $this->financeiroPorUsuario($authenticate);
        }

        $totalizador = [
            'enviado_valor_total' => 0,
            'recebido_valor_total' => 0,
            'recebido_pacote_total' => 0,
            'recebido_bonus_total' => 0,
            'lucro' => 0,

            'proximo_receber_total' => 0,
            'proximo_receber_pacote_total' => 0,
            'proximo_receber_bonus_total' => 0,
            'proximo_enviar_total' => 0,
            'proximo_lucro' => 0,
        ];

        foreach ($items as $item) {
            $totalizador['enviado_valor_total'] += Arr::get($item, 'enviado.valor_total');

            $totalizador['recebido_valor_total'] += Arr::get($item, 'recebido.valor_total');
            $totalizador['recebido_pacote_total'] += Arr::get($item, 'recebido.pacote_total');
            $totalizador['recebido_bonus_total'] += Arr::get($item, 'recebido.bonus_total');

            $totalizador['proximo_receber_total'] += Arr::get($item, 'proximo.receber_total');
            $totalizador['proximo_receber_pacote_total'] += Arr::get($item, 'proximo.receber_pacote_total');
            $totalizador['proximo_receber_bonus_total'] += Arr::get($item, 'proximo.receber_bonus_total');
            $totalizador['proximo_enviar_total'] += Arr::get($item, 'proximo.enviar_total');
        }

        $totalizador['proximo_lucro'] = $totalizador['proximo_receber_total'] - $totalizador['proximo_enviar_total'];
        $totalizador['lucro'] = $totalizador['recebido_valor_total'] - $totalizador['enviado_valor_total'];

        return [
            'totalizador' => $totalizador,
            'items' => $items,
        ];
    }

    /**
     * @param $authenticate
     * @return array
     * @throws InvalidTokenException
     */
    private function financeiroPorUsuario($authenticate): array
    {
        $result = [
            'authenticate' => $authenticate,
            'enviado' => $this->usuarioEnviado($authenticate),
        ];

        $result = array_merge($result, $this->usuarioRecebimento($authenticate));

        $lucro = Arr::get($result, 'recebido.valor_total') - Arr::get($result, 'enviado.valor_total');
        $result['lucro'] = round($lucro, 2);

        return $result;
    }

    /**
     * @param $authenticate
     * @return array
     * @throws InvalidTokenException
     */
    private function usuarioEnviado($authenticate): array
    {
        /** @var DoacoesEnviadasRepository $doacoesEnviadasRepository */
        $doacoesEnviadasRepository = app(DoacoesEnviadasRepository::class);
        $doacoesEnviadasRepository->setAuthenticate($authenticate);
        $doacoesEnviadas = $doacoesEnviadasRepository->get();

        $valorTotal = 0;
        foreach ($doacoesEnviadas->doar as $doarItem) {
            $valorTotal += $doarItem->ped_total;
        }

        return [
            'valor_total' => $valorTotal,
            'quantidade_total' => $doacoesEnviadas->doar->count()
        ];
    }

    /**
     * @param $authenticate
     * @return array
     * @throws InvalidTokenException
     */
    private function usuarioRecebimento($authenticate): array
    {
        $enviado = [
            'valor_total' => 0,
            'bonus_total' => 0,
            'quantidade_total' => 0,
            'pacote_total' => 0,
            'valor_medio' => 0,
        ];

        $proximo = [
            'receber_total' => 0,
            'receber_pacote_total' => 0,
            'receber_bonus_total' => 0,
            'enviar_total' => 0,
            'lucro' => 0,
        ];

        /** @var DoacoesRecebidasRepository $doacoesRecebidasRepository */
        $doacoesRecebidasRepository = app(DoacoesRecebidasRepository::class);
        $doacoesRecebidasRepository->setAuthenticate($authenticate);

        $doacoesRecebidas = $doacoesRecebidasRepository->get();
        foreach ($doacoesRecebidas->doar as $doarItem) {
            if ($doarItem->status == true) {
                $proximo['receber_total'] += round($doarItem->valor_total, 2);
                $proximo['receber_pacote_total'] += round($doarItem->pacote_investimento_valor, 2);
                $proximo['receber_bonus_total'] += round($doarItem->valor_total - $doarItem->pacote_investimento_valor, 2);
                $proximo['enviar_total'] += round($doarItem->pacote_valor, 2);
                $proximo['id'] = $doarItem->id;

                continue;
            }

            $enviado['quantidade_total']++;
            $enviado['valor_total'] += round($doarItem->valor_total, 2);
            $enviado['pacote_total'] += round($doarItem->pacote_investimento_valor, 2);
            $enviado['bonus_total'] += round($doarItem->valor_total - $doarItem->pacote_investimento_valor, 2);
        }

        if ($enviado['quantidade_total'] !== 0) {
            $valorMedio = $enviado['valor_total'] / $enviado['quantidade_total'];
            $enviado['valor_medio'] = round($valorMedio, 2);
        }

        $proximo['lucro'] = $proximo['receber_total'] - $proximo['enviar_total'];
        return [
            'recebido' => $enviado,
            'proximo' => $proximo,
        ];
    }


}
