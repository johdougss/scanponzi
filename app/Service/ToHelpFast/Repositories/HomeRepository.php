<?php

namespace App\Service\ToHelpFast\Repositories;

use App\Service\ToHelpFast\Exceptions\InvalidTokenException;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Cache\CacheManager;
use Illuminate\Support\Arr;
use stdClass;

class HomeRepository
{
    use TokenTrait;

    /**
     * @return stdClass
     */
    public function get()
    {
        $key = vsprintf('%s;%s;%s', [
            'tohelpfast',
            'home',
            Arr::get($this->authenticate, 'username')
        ]);

        /** @var CacheManager $cache */
        $cache = app('cache');
        return $cache->store('array')->rememberForever($key, function () {

            $callback = function () {
                $url = vsprintf('%s/Bo/home/token/%s?time=1567362509372', [
                    $this->getHost(),
                    $this->getToken(),
                ]);

                $options = [
                    'headers' => [
                        'Accept' => 'application/json, text/plain, */*',
                        'Origin' => 'https://tohelpfast.com',
                        'Content-Type' => 'application/x-www-form-urlencoded',
//                    'Referer' => 'https://tohelpfast.com/bo.php',
                        'Sec-Fetch-Mode' => 'cors',
                        'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36',
                    ],
                ];

                $client = new Client();
                // echo $url . PHP_EOL;
                return $client->get($url, $options);
            };

            $response = $this->validateAuthenticateResponse($callback);
            $content = json_decode((string)$response->getBody());


            return $this->map($content);
        });
    }

    /**
     * @param $entity
     * @return mixed
     * @throws Exception
     */
    private function map($entity)
    {
        $pacoteNome = $entity->usuariox->bn_nome;
        $pacote = $this->getPacoteByNome($pacoteNome);

        $entity->usuario = new stdClass();
        $entity->usuario->pacote_nome = $pacoteNome;
        $entity->usuario->pacote_id = $pacote['id'];
        $entity->usuario->pacote_valor = $pacote['valor'];

        return $entity;
    }

    /**
     * @param $pacoteNome
     * @return mixed
     * @throws Exception
     */
    private function getPacoteByNome($pacoteNome)
    {
        $items = [
            'Pacote 1' => [
                'id' => 1,
                'valor' => 25
            ],
            'Pacote 2' => [
                'id' => 2,
                'valor' => 50
            ],
            'Pacote 3' => [
                'id' => 3,
                'valor' => 100
            ],
            'Pacote 4' => [
                'id' => 4,
                'valor' => 200
            ],
            'Pacote 5' => [
                'id' => 5,
                'valor' => 300
            ],
            'Pacote 6' => [
                'id' => 6,
                'valor' => 600
            ],
            'Pacote 7' => [
                'id' => 7,
                'valor' => 1000
            ],
            'Pacote 8' => [
                'id' => 8,
                'valor' => 1500
            ],
            'Pacote 9' => [
                'id' => 9,
                'valor' => 3000
            ],
            'Pacote 10' => [
                'id' => 10,
                'valor' => 5000
            ],
            'Pacote 11' => [
                'id' => 11,
                'valor' => 7000
            ],
            'Pacote 12' => [
                'id' => 12,
                'valor' => 10000
            ],
        ];

        if (!isset($items[$pacoteNome])) {
            throw new Exception('Pacote não identificado. ' . $pacoteNome);
        }

        return $items[$pacoteNome];
    }

}
