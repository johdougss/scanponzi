<?php

namespace App\Service\ToHelpFast\Repositories;

use App\Service\ToHelpFast\Exceptions\InvalidTokenException;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Support\Collection;

class DoacoesEnviadasRepository
{
    use TokenTrait;

    /**
     * @return mixed
     * @throws InvalidTokenException
     */
    public function get()
    {
        $callback = function () {
            $url = vsprintf('%s/Bo/minhasDoacoes/token/%s?time=1566960904646', [
                $this->getHost(),
                $this->getToken(),
            ]);

            $options = [
                'headers' => [
                    'Accept' => 'application/json, text/plain, */*',
                    'Content-Type' => 'application/x-www-form-urlencoded',
                    'Referer' => 'https://tohelpfast.com/bo.php',
                    'Sec-Fetch-Mode' => 'cors',
                    'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36',
                ],
            ];

            $client = new Client();
            // echo $url . PHP_EOL;
            return $client->get($url, $options);
        };

        $response = $this->validateAuthenticateResponse($callback);
        $entity = json_decode((string)$response->getBody());

        return $this->map($entity);
    }

    /**
     * @param $entity
     * @return mixed
     */
    private function map($entity)
    {
        /** @var HomeRepository $homeRepository */
        $homeRepository = app(HomeRepository::class);
        $homeRepository->setAuthenticate($this->authenticate);
        $home = $homeRepository->get();

        $entity->doar = new Collection($entity->doar);
        $entity->doar->map(function ($item) use ($home) {
            $item->ped_total = (float)$item->ped_total;
            $item->ped_valor = (float)$item->ped_valor;

            $datetime = $item->data . $item->hora;
            $item->date = Carbon::createFromFormat('Y-m-dH:i', $datetime);
            unset($item->hora);
            unset($item->data);

            $item->pacote_id = $home->usuario->pacote_id;
            $item->pacote_valor = $home->usuario->pacote_valor * 0.1;

            return $item;
        });

        return $entity;
    }
}
