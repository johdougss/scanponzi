<?php

namespace App\Service\ToHelpFast\Repositories;

use App\Service\ToHelpFast\Exceptions\InvalidTokenException;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use stdClass;

class ListarRecebidasRepository
{
    use TokenTrait;

    /**
     * @param $number
     * @return stdClass
     * @throws InvalidTokenException
     */
    public function getByNumber($number)
    {
        $callback = function () use ($number) {
            $url = vsprintf('%s/Bo/listarRecebidas/token/%s?time=1566960904646', [
                $this->getHost(),
                $this->getToken(),
            ]);

            $options = [
                RequestOptions::HEADERS => [
                    'Accept' => 'application/json, text/plain, */*',
                    'Content-Type' => 'application/x-www-form-urlencoded',
//                    'Referer' => 'https://tohelpfast.com/bo.php',
                    'Sec-Fetch-Mode' => 'cors',
                    'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36',
                ],
                RequestOptions::BODY => $number
            ];

            $client = new Client();
//            echo $url . '---' . $number . PHP_EOL;
            return $client->post($url, $options);
        };

        $response = $this->validateAuthenticateResponse($callback);
        $content = json_decode((string)$response->getBody());

        return $this->map($content);
    }

    /**
     * @param $entity
     * @return mixed
     */
    private function map($entity)
    {
        return $entity;
    }
}
