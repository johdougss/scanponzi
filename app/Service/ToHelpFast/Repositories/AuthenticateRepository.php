<?php

namespace App\Service\ToHelpFast\Repositories;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Config;

class AuthenticateRepository
{
    private $authenticate;

    /**
     * @return Collection
     */
    public function get()
    {
        if ($this->authenticate === null) {
            $items = Config::get('authenticate_user');
            $this->authenticate = new Collection($items);
        }

        return $this->authenticate;
    }
}
