<?php

namespace App\Service\ToHelpFast\Repositories;

use Exception;
use GuzzleHttp\Client;
use Illuminate\Cache\CacheManager;
use Illuminate\Support\Arr;
use stdClass;

class PerfilRepository
{
    use TokenTrait;

    /**
     * @return stdClass
     */
    public function get()
    {
        $key = vsprintf('%s;%s;%s', [
            'tohelpfast',
            'home',
            Arr::get($this->authenticate, 'username')
        ]);

        /** @var CacheManager $cache */
        $cache = app('cache');
        return $cache->store('array')->rememberForever($key, function () {

            $callback = function () {
                $url = vsprintf('%s/Bo/main/token/%s?time=1566965731707', [
                    $this->getHost(),
                    $this->getToken(),
                ]);

                $options = [
                    'headers' => [
                        'Accept' => 'application/json, text/plain, */*',
                        'Origin' => 'https://tohelpfast.com',
                        'Content-Type' => 'application/x-www-form-urlencoded',
//                    'Referer' => 'https://tohelpfast.com/bo.php',
                        'Sec-Fetch-Mode' => 'cors',
                        'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36',
                    ],
                ];

                $client = new Client();
                // echo $url . PHP_EOL;
                return $client->get($url, $options);
            };

            $response = $this->validateAuthenticateResponse($callback);
            $content = json_decode((string)$response->getBody());


            return $this->map($content);
        });
    }

    /**
     * @param $entity
     * @return mixed
     * @throws Exception
     */
    private function map($entity)
    {
        return $entity;
    }
}
