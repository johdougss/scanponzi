<?php

namespace App\Service\ToHelpFast\Repositories;

trait AuthenticateTrait
{
    protected $authenticate;

    /**
     * @param mixed $authenticate
     */
    public function setAuthenticate($authenticate): void
    {
        $this->authenticate = $authenticate;
    }
}
