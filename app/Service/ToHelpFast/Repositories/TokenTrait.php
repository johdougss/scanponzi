<?php

namespace App\Service\ToHelpFast\Repositories;

use App\Service\ToHelpFast\Exceptions\InvalidTokenException;
use Closure;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Illuminate\Cache\CacheManager;
use Illuminate\Support\Arr;

trait TokenTrait
{
    use AuthenticateTrait;

    private function getToken()
    {
        /** @var CacheManager $cache */
        $cache = app('cache');
        $token = $cache->rememberForever($this->getTokenCacheKey(), function () {
            return $this->requestToken($this->authenticate);
        });

        return $token;
    }

    private function clearToken()
    {
        /** @var CacheManager $cache */
        $cache = app('cache');
        $cache->forget($this->getTokenCacheKey());
    }

    private function getHost()
    {
        return 'https://api.tohelpfast.com';
    }

    private function getTokenCacheKey()
    {
        $key = vsprintf('%s;%s;%s', [
            'tohelpfast',
            'token',
            Arr::get($this->authenticate, 'username')
        ]);

        return $key;
    }

    /**
     * @param string $content
     * @throws InvalidTokenException
     */
    private function validateAuthenticate(string $content)
    {
        if ($content === 'null') {
            throw new InvalidTokenException();
        }

        $content = json_decode($content);
        if (isset($content->status) && $content->status === false) {
            throw new InvalidTokenException();
        }

        return;
    }

    /**
     * @param $authenticate
     * @return mixed
     * @throws Exception
     */
    private function requestToken($authenticate): string
    {
        $url = vsprintf('%s/Bo/login', [
            $this->getHost()
        ]);

        $body = json_encode([
            'username' => Arr::get($authenticate, 'username'),
            'password' => Arr::get($authenticate, 'password'),
        ]);

        $options = [
            'headers' => [
                'Accept' => 'application/json, text/plain, */*',
                'Content-Type' => 'application/x-www-form-urlencoded',
//                'Referer' => 'https://tohelpfast.com/login.html',
                'Sec-Fetch-Mode' => 'cors',
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36',
            ],
            RequestOptions::BODY => $body
        ];

        $client = new Client();
        $response = $client->post($url, $options);
        $content = json_decode($response->getBody()->getContents(), true);

        $token = Arr::get($content, 'dados.token');

        if ($token === null) {
            throw new Exception('token não encontrado');
        }

        return $token;
    }

    /**
     * @param Closure $callback
     * @return mixed
     * @throws InvalidTokenException
     */
    private function validateAuthenticateResponse(Closure $callback)
    {
        try {
            $response = $callback();
            $content = (string)$response->getBody();
            $this->validateAuthenticate($content);
        } catch (InvalidTokenException $exception) {
            $this->clearToken();

            $response = $callback();
            $content = (string)$response->getBody();
            $this->validateAuthenticate($content);
        }

        return $response;
    }

}
