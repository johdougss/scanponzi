<?php

namespace App\Service\ToHelpFast\Repositories;

use App\Service\ToHelpFast\Exceptions\InvalidTokenException;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Support\Collection;
use stdClass;

class DoacoesRecebidasRepository
{
    use TokenTrait;

    /**
     * @return stdClass
     * @throws InvalidTokenException
     */
    public function get()
    {
        $callback = function () {
            $url = vsprintf('%s/Bo/minhasDoacoesRecebidas/token/%s?time=1566960904646', [
                $this->getHost(),
                $this->getToken(),
            ]);

            $options = [
                'headers' => [
                    'Accept' => 'application/json, text/plain, */*',
                    'Content-Type' => 'application/x-www-form-urlencoded',
//                    'Referer' => 'https://tohelpfast.com/bo.php',
                    'Sec-Fetch-Mode' => 'cors',
                    'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36',
                ],
            ];

            $client = new Client();
            // echo $url . PHP_EOL;
            return $client->get($url, $options);
        };

        $response = $this->validateAuthenticateResponse($callback);
        $content = json_decode((string)$response->getBody());

        return $this->map($content);
    }

    /**
     * @param $entity
     * @return mixed
     */
    private function map($entity)
    {
        /** @var HomeRepository $homeRepository */
        $homeRepository = app(HomeRepository::class);
        $homeRepository->setAuthenticate($this->authenticate);
        $home = $homeRepository->get();

        $entity->doar = new Collection($entity->doar);
        $entity->doar->map(function ($item) use ($home) {
            $item->valor_total = (float)$item->valor_total;
            $item->valor = (float)$item->valor;

            $datetime = $item->data . $item->hora;
            $item->date = Carbon::createFromFormat('Y-m-dH:i', $datetime);
            unset($item->hora);
            unset($item->data);

            $item->pacote = $home->usuario->pacote_id;
            $item->pacote_valor = $home->usuario->pacote_valor;
            $item->pacote_investimento_valor = $home->usuario->pacote_valor * 1.1;

            return $item;
        });

        return $entity;
    }
}

//doador encontrado e confirmado automatico
//{
//    "id": 10704,
//            "valor_total": "28.00",
//            "valor": "0.00",
//            "status": false,
//            "qtd": 1,
//            "qtd_confirmado": 1,
//            "data": "2019-09-03",
//            "hora": "03:07"
//        }

//doador encontrado - pagamento realizado com comprovante e ainda não confirmado
//{
//    "id": 10699,
//            "valor_total": "35.00",
//            "valor": "0.00",
//            "status": true,
//            "qtd": 1,
//            "qtd_confirmado": 0,
//            "data": "2019-09-03",
//            "hora": "03:04"
//        }

//doador encontrado - pagamento nao realizado
//{
//    "id": 10680,
//            "valor_total": "28.00",
//            "valor": "0.00",
//            "status": true,
//            "qtd": 1,
//            "qtd_confirmado": 0,
//            "data": "2019-09-03",
//            "hora": "02:48"
//        }
//

//doador encontrado - usuario enviou o comprovante ( nao fez automatico)
//{
//    "id": 10680,
//            "valor_total": "28.00",
//            "valor": "0.00",
//            "status": true,
//            "qtd": 1,
//            "qtd_confirmado": 0,
//            "data": "2019-09-03",
//            "hora": "02:48"
//        }
//doador nao encontrado
//{
//    "id": 11334,
//            "valor_total": "27.50",
//            "valor": "27.50",
//            "status": true,
//            "qtd": 0,
//            "qtd_confirmado": 0,
//            "data": "2019-09-03",
//            "hora": "17:10"
//        }

//https://tohelpfast.com/img/comprovantes/vittafree_1568228760934.png
//https://tohelpfast.com/img/comprovantes/abimaelrosario_1567564184485.png
