<?php

namespace App\Service\ToHelpFast;

use App\Service\ToHelpFast\Exceptions\InvalidTokenException;
use App\Service\ToHelpFast\Repositories\AuthenticateRepository;
use App\Service\ToHelpFast\Repositories\ListarRecebidasRepository;

class IndiceService
{

    /**
     * @return false|int|string
     */
    public function getIndiceLast()
    {
        $value = file_get_contents(__DIR__ . '/value.txt');
        if ($value === false) {
            return 1;
        }

        return (int)$value;
    }

    public function saveIndiceLast(int $indice)
    {
        file_put_contents(__DIR__ . '/value.txt', $indice);
    }

    /**
     * @param int $indice
     * @return bool
     * @throws InvalidTokenException
     */
    public function isDoador(int $indice): bool
    {
        /** @var AuthenticateRepository $authenticateRepository */
        $authenticateRepository = app(AuthenticateRepository::class);
        $authenticates = $authenticateRepository->get();
        $authenticate = $authenticates->first();

        $listaRecebidasRepository = new ListarRecebidasRepository();
        $listaRecebidasRepository->setAuthenticate($authenticate);
        $item = $listaRecebidasRepository->getByNumber($indice);

        return $item->status === true && count($item->listarR) !== 0;
    }

    /**
     * @return false|float|int|string
     * @throws InvalidTokenException
     */
    public function discoveryIndice()
    {
        $indice = $this->getIndiceLast();

        if ($this->isDoador($indice) === false) {
            $this->saveIndiceLast($indice);
            return $indice;
        }

        $increment = 100;
        while (true) {
            //$this->info('while 1');
            $indice += $increment;

            if ($this->isDoador($indice) === false) {
                $indice -= $increment;
                $increment = $increment / 10;  //10

                while (true) {
                    //$this->info(' while 2');
                    $indice += $increment;

                    if ($this->isDoador($indice) === false) {
                        $indice -= $increment;
                        $increment = $increment / 10; //1

                        while (true) {
                            //$this->info('  while 3');
                            $indice += $increment;

                            if ($this->isDoador($indice) === false) {
                                break 3;
                            }

                            $this->sleep();
                        }
                    }
                    $this->sleep();
                }
            }
            $this->sleep();
        }

        $this->saveIndiceLast($indice);
        return $indice;
    }

    public function sleep(): void
    {
        usleep(500 * 1000); //500ms
    }
}
