<?php

namespace App\Service\ToHelpFast;

use App\Service\ToHelpFast\Exceptions\InvalidTokenException;
use App\Service\ToHelpFast\Repositories\AuthenticateRepository;
use App\Service\ToHelpFast\Repositories\ListarRecebidasRepository;

class IndiceContagemService
{
    /**
     * @param int $indice
     * @return array
     * @throws InvalidTokenException
     */
    public function detailDoacaoByIndice(int $indice): array
    {
        /** @var AuthenticateRepository $authenticateRepository */
        $authenticateRepository = app(AuthenticateRepository::class);
        $authenticates = $authenticateRepository->get();
        $authenticate = $authenticates->first();

        $listaRecebidasRepository = new ListarRecebidasRepository();
        $listaRecebidasRepository->setAuthenticate($authenticate);
        $response = $listaRecebidasRepository->getByNumber($indice);

        $result = [
            'quantidade_total' => 0,
            'pagamento_realizado_total' => 0,
            'valor_total' => 0,
            'items' => [],
        ];

        foreach ($response->listarR as $item) {
            $result['quantidade_total']++;
            $result['valor_total'] += (float)$item->valor;

            if ($item->status === true) {
                $result['pagamento_realizado_total']++;
            }

            $result['items'][] = $item;
        }

        return $result;
    }

    /**
     * @param $inicio
     * @param $fim
     * @return array
     * @throws InvalidTokenException
     */
    public function run($inicio, $fim)
    {
        $result = [
            'quantidade_total' => 0,
            'pagamento_realizado_total' => 0,
            'valor_total' => 0,
            'inicio' => $inicio,
            'fim' => $fim,
            'items' => []
        ];

        for ($i = $inicio; $i < $fim; $i++) {
            $item = $this->detailDoacaoByIndice($i);
            $result['quantidade_total'] += $item['quantidade_total'];
            $result['valor_total'] += $item['valor_total'];
            $result['pagamento_realizado_total'] += $item['pagamento_realizado_total'];
            $result['items'] [] = $item;

            $this->sleep();
        }

        return $result;
    }

    public function sleep(): void
    {
        usleep(500 * 1000); //500ms
    }
}
