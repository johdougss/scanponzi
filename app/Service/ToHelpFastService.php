<?php
//
//namespace App\Service;
//
//use GuzzleHttp\Client;
//use Illuminate\Cache\CacheManager;
//use Illuminate\Support\Arr;
//use Illuminate\Support\Collection;
//use stdClass;
//
//class ToHelpFastService
//{
//    protected $authenticate;
//
//    /**
//     * @param mixed $authenticate
//     */
//    public function setAuthenticate($authenticate): void
//    {
//        $this->authenticate = $authenticate;
//    }
//
//    private function getToken()
//    {
//        /** @var CacheManager $cache */
//        $cache = app('cache');
//        $token = $cache->rememberForever($this->getTokenCacheKey(), function () {
//            return $this->requestToken($this->authenticate);
//        });
//
//        return $token;
//    }
//
//    private function clearToken()
//    {
//        /** @var CacheManager $cache */
//        $cache = app('cache');
//        $cache->forget($this->getTokenCacheKey());
//    }
//
////    private function $this->authenticate
////    {
////        $user1 = [
////            'username' => 'johdougss',
////            'password' => 'Joh123321.',
////        ];
////
////        $user2 = [
////            'username' => 'johdougss5',
////            'password' => 'joh123321',
////        ];
////
////        return $user1;
////    }
//
//
//
//    /**
//     * @return stdClass
//     */
//    public function getDoacoesRecebidas()
//    {
//        $callback = function () {
//            $url = vsprintf('%s/Bo/minhasDoacoesRecebidas/token/%s?time=1566960904646', [
//                $this->getHost(),
//                $this->getToken(),
//            ]);
//
//            $options = [
//                'headers' => [
//                    'Accept' => 'application/json, text/plain, */*',
//                    'Content-Type' => 'application/x-www-form-urlencoded',
//                    'Referer' => 'https://tohelpfast.com/bo.php',
//                    'Sec-Fetch-Mode' => 'cors',
//                    'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36',
//                ],
//            ];
//
//            $client = new Client();
//
//
//            $response = $client->get($url, $options);
//            $content = $response->getBody()->getContents();
//
//            $this->checkToken($content);
//            $result = json_decode($content);
//
//            return $result;
//        };
//
//        try {
//            return $callback();
//        } catch (InvalidTokenException $e) {
//            $this->clearToken();
//            return $callback();
//        }
//    }
//
//    private function getHost()
//    {
//        return 'https://api.tohelpfast.com';
//    }
//
//    private function getTokenCacheKey()
//    {
//        $key = vsprintf('%s;%s;%s', [
//            'tohelpfast',
//            'token',
//            Arr::get($this->authenticate, 'username')
//        ]);
//
//        return $key;
//    }
//
//    /**
//     * @return string
//     * @throws InvalidTokenException
//     */
//    public function statusUltimaDoacaoRecebida()
//    {
//        $response = $this->getDoacoesRecebidas();
//
//        $doacoes = new Collection(Arr::get($response, 'doar', []));
//        $doacoes = $doacoes->sortBy(function ($doacao) {
//            return Arr::get($doacao, 'id');
//        });
//
//        $doacao = $doacoes->last();
////        $quantidade = Arr::get($doacao, 'qtd');
////        $quantidadeConfirmados = Arr::get($doacao, 'qtd_confirmado');
////
////        if ($quantidade === 0) {
////            return 'doador ainda não identificado';
////        }
////
////        if ($quantidade > 0 && $quantidade > $quantidadeConfirmados) {
////            $valor = (float)Arr::get($doacao, 'valor');
////            if ($valor === 0) {
////                return vsprintf('aguardando doações [%s/%s]', [$quantidadeConfirmados, $quantidade]);
////            } else {
////                return vsprintf('aguardando doações [%s/%s]', [$quantidadeConfirmados, $quantidade]);
////            }
////        }
////
////        return 'doação recebida';
//    }
//
//    /**
//     * @param string $content
//     * @throws InvalidTokenException
//     */
//    private function checkToken(string $content)
//    {
//        if ($content !== 'null') {
//            return;
//        }
//
//        throw new InvalidTokenException();
//    }
//
//
//}
