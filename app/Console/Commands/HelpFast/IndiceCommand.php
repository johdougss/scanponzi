<?php

namespace App\Console\Commands\HelpFast;

use App\Service\ToHelpFast\Exceptions\InvalidTokenException;
use App\Service\ToHelpFast\IndiceService;
use Exception;
use Illuminate\Console\Command;

class IndiceCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'helpfast:indice';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @throws InvalidTokenException
     * @throws Exception
     */
    public function handle()
    {
        /** @var IndiceService $indiceService */
        $indiceService = app(IndiceService::class);
        $indice = $indiceService->discoveryIndice();


        $this->info('proximo indice: ' . $indice);
    }


}
