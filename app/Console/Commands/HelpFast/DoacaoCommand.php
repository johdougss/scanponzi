<?php

namespace App\Console\Commands\HelpFast;

use App\Service\ToHelpFast\DoacaoService;
use App\Service\ToHelpFast\Exceptions\InvalidTokenException;
use Carbon\Carbon;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;

class DoacaoCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'helpfast:doacao';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @throws InvalidTokenException
     * @throws Exception
     */
    public function handle()
    {
        /** @var DoacaoService $doacaoService */
        $doacaoService = app(DoacaoService::class);
        $this->show($doacaoService->get());
    }

    /**
     * @param $results
     * @throws Exception
     */
    private function show($results)
    {
        $count = 0;
        foreach ($results as $result) {
            $count++;

            $this->line(vsprintf('<fg=yellow>%s - username: %s</>', [$count, Arr::get($result, 'authenticate.username')]));

            $this->line(' <fg=green>status</>');
            $statusId = Arr::get($result, 'status_id');
            $doacao = Arr::get($result, 'doacao');


            $this->line($this->getStatusLine($doacao, $statusId));
            /** @var Carbon $date */

            $date = $doacao->date;

            $dateBrasil = $date->copy();
            $this->line(vsprintf('   <fg=green>ultima: %s </><fg=blue>(%s BR)</>  <fg=yellow>%s</>', [
                $date,
                $dateBrasil->setTimezone('	America/Sao_Paulo'),
                $dateBrasil->format('Ymd') === Carbon::now()->format('Ymd') ? '*' : '',
            ]));

            $this->line('');
        }
    }

    /**
     * @param $doacao
     * @param $statusId
     * @return string
     * @throws Exception
     */
    private function getStatusLine($doacao, $statusId)
    {
        return vsprintf('   <fg=%s>%s</>', [($statusId === 1 ? 'cyan' : 'green'), $this->getStatusDescription($doacao, $statusId)]);
    }

    /**
     * @param $doacao
     * @param $statusId
     * @return string
     * @throws Exception
     */
    private function getStatusDescription($doacao, $statusId): string
    {
        if ($statusId === 1) {
            return 'doação realizada';
        }

        if ($statusId === 2) {
            return 'nenhum doador encontrado';
        }

        if ($statusId === 3) {
            return vsprintf('doador encontrado [%s/%s] - verificar comprovante', [$doacao->qtd_confirmado, $doacao->qtd]);
        }

        throw new Exception('status nao identificado: ' . $statusId);
    }
}
