<?php

namespace App\Console\Commands\HelpFast;

use App\Service\ToHelpFast\IndiceContagemService;
use Carbon\Carbon;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class IndiceContagemCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'helpfast:indice-contagem
      {--inicio}
      {--fim}
    ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @throws Exception
     */
    public function handle()
    {
        $inicio = $this->option('inicio');
        $fim = $this->option('fim');

        $this->validate($inicio, $fim);

        /** @var IndiceContagemService $indiceContagemService */
        $indiceContagemService = app(IndiceContagemService::class);
        $result = $indiceContagemService->run($inicio, $fim);

        $this->show($result);
    }

    private function show(array $result)
    {
        $this->line(vsprintf('<fg=yellow>Horario %s</>', [Carbon::now()->format('d/m/Y H:i:s')]));

        $this->line(' <fg=green>Ordens</>');
        $this->line(vsprintf('   inicio: <fg=green>%s</>', [Arr::get($result, 'inicio')]));
        $this->line(vsprintf('   fim: <fg=green>%s</>', [Arr::get($result, 'fim')]));

        $quantidade = Arr::get($result, 'fim') - Arr::get($result, 'inicio');
        $this->line(vsprintf('   quantidade: <fg=green>%s</>', [$quantidade]));
        $this->line('');

        $this->line(' <fg=green>Doações realizadas</>');
        $this->line(vsprintf('   valor total: <fg=green>%s</>', [Arr::get($result, 'valor_total')]));
        $this->line(vsprintf('   quantidade total: <fg=green>%s</>', [Arr::get($result, 'quantidade_total')]));
        $this->line(vsprintf('   quantidade de pagamentos já realizados: <fg=green>%s</>', [Arr::get($result, 'pagamento_realizado_total')]));
        $this->line('');
    }

    /**
     * @param $inicio
     * @param $fim
     */
    private function validate($inicio, $fim): void
    {
        $arr = [
            'inicio' => $inicio,
            'fim' => $fim,
        ];
        $validator = Validator::make($arr, [
            'inicio' => 'required|int',
            'fim' => 'required|int',
        ]);

        $validator->validate();

//        if ($validator->fails()) {
//            foreach ($validator->errors()->messages() as $key => $messages) {
//                $this->error($key);
//                foreach ($messages as $message) {
//                    $this->error('  ' . $message);
//                }
//            }
//        }
    }


}
