<?php

namespace App\Console\Commands\HelpFast;

use App\Service\ToHelpFast\Exceptions\InvalidTokenException;
use App\Service\ToHelpFast\Repositories\AuthenticateRepository;
use App\Service\ToHelpFast\Repositories\PerfilRepository;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;

class SaudeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'helpfast:saude';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @throws InvalidTokenException
     */
    public function handle()
    {
        /** @var AuthenticateRepository $authenticateRepository */
        $authenticateRepository = app(AuthenticateRepository::class);
        $authenticates = $authenticateRepository->get();
        $authenticate = $authenticates->first();

        /** @var PerfilRepository $perfilRepository */
        $perfilRepository = app(PerfilRepository::class);
        $perfilRepository->setAuthenticate($authenticate);
        $result = $perfilRepository->get();
        $this->show($result, $authenticate);
    }

    /**
     * @param $result
     * @param $authenticate
     */
    private function show($result, $authenticate): void
    {
        $this->line('<fg=yellow>username: ' . Arr::get($authenticate, 'username') . '</>');
        $this->line('<fg=blue>fluxo de caixa</>');

        $entrada = round($result->money->entrada, 2);
        $saida = round($result->money->saida, 2);
        $porcentagem = round($saida * 100 / $entrada, 2);

        $this->line(vsprintf('     entrada: <fg=green>%s</>', [$entrada]));
        $this->line(vsprintf('       saida: <fg=green>%s</>', [$saida]));
        $this->line(vsprintf(' porcentagem: <fg=green>%s%%</>', [$porcentagem]));

        $content = implode([$entrada, $saida, $porcentagem, Carbon::now()->format('YmdHis')], ',') . PHP_EOL;
        $filename = __DIR__ . '/history_fluxo.txt';
        file_put_contents($filename, $content, FILE_APPEND);

//        $content = file_get_contents($filename);
//
//        foreach (explode(PHP_EOL, $content) as $line) {
//            $item = explode(',', $line);
//        }
    }

}
