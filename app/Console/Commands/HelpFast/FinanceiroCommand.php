<?php

namespace App\Console\Commands\HelpFast;

use App\Service\ToHelpFast\Exceptions\InvalidTokenException;
use App\Service\ToHelpFast\FinanceiroService;
use App\Service\ToHelpFast\IndiceService;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;

class FinanceiroCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'helpfast:financeiro';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @throws InvalidTokenException
     */
    public function handle()
    {
        /** @var FinanceiroService $financeiroService */
        $financeiroService = app(FinanceiroService::class);

        /** @var IndiceService $indiceService */
        $indiceService = app(IndiceService::class);
        $proximaOrdemId = $indiceService->discoveryIndice();

        $this->show($financeiroService->financeiroGeral(), $proximaOrdemId);
    }

    /**
     * @param $results
     * @param $proximaOrdemId
     */
    private function show($results, $proximaOrdemId): void
    {
        $this->line(vsprintf('<fg=yellow>Horario %s</>', [Carbon::now()->format('d/m/Y H:i:s')]));

        $count = 0;
        foreach ($results['items'] as $item) {
            $count++;

            $this->line(vsprintf('<fg=yellow>%s - username: %s</>', [$count, Arr::get($item, 'authenticate.username')]));

            $this->line(' <fg=green>enviadas</>');
            $this->line('   valor total: <fg=green>' . Arr::get($item, 'enviado.valor_total') . '</>');
            $this->line('   quantidade total: <fg=green>' . Arr::get($item, 'enviado.quantidade_total') . '</>');

            $this->line(' <fg=green>recebidas</>');
            $this->line(vsprintf('   valor total: <fg=green>%s</>', [Arr::get($item, 'recebido.valor_total'),]));
            $this->line(vsprintf('     bonus: <fg=green>%s</>', [Arr::get($item, 'recebido.bonus_total')]));
            $this->line(vsprintf('     pacote: <fg=green>%s</>', [Arr::get($item, 'recebido.pacote_total')]));
            $this->line(vsprintf('   valor medio: <fg=green>%s</>', [Arr::get($item, 'recebido.valor_medio'),]));
            $this->line('   quantidade total: <fg=green>' . Arr::get($item, 'recebido.quantidade_total') . '</>');
            $this->line(' <fg=green>lucro</>');
            $this->line('   valor: <fg=green>' . Arr::get($item, 'lucro') . '</>');

            $this->line(' <fg=green>próximo</>');

            $proximoId = Arr::get($item, 'proximo.id');
            $contagemIndice = (int)$proximoId - $proximaOrdemId;
            $contagemIndice = $contagemIndice > 0 ? $contagemIndice : 0;
            $this->line(vsprintf('        id: <fg=green>%s ( %s faltantes)</>', [$proximoId, $contagemIndice]));
            $this->line('   receber: <fg=green>' . Arr::get($item, 'proximo.receber_total') . '</>');
            $this->line('');
            $this->line('');
        }

        $dolar = 4;

        $this->line('<fg=blue>valor total</>');
        $this->line('   enviadas: <fg=green>' . Arr::get($results, 'totalizador.enviado_valor_total') . '</>');
        $this->line('   recebidas: <fg=green>' . Arr::get($results, 'totalizador.recebido_valor_total') . '</>');
        $this->line('     bonus: <fg=green>' . Arr::get($results, 'totalizador.recebido_bonus_total') . '</>');
        $this->line('     pacote: <fg=green>' . Arr::get($results, 'totalizador.recebido_pacote_total') . '</>');
        $lucro = Arr::get($results, 'totalizador.lucro');
        $this->line(vsprintf('   lucro: <fg=magenta>%s (%s)</>', [$lucro, $lucro * $dolar]));
        $this->line('');


        $this->line('<fg=blue>próximo</>');
        $proximoEnviar = Arr::get($results, 'totalizador.proximo_enviar_total');
        $this->line(vsprintf('   enviar: <fg=green>%s (%s)</>', [$proximoEnviar, $proximoEnviar * $dolar]));
        $proximoReceber = Arr::get($results, 'totalizador.proximo_receber_total');
        $this->line(vsprintf('   receber: <fg=green>%s (%s)</>', [$proximoReceber, $proximoReceber * $dolar]));
        $this->line(vsprintf('     bonus: <fg=green>%s</>', [Arr::get($results, 'totalizador.proximo_receber_bonus_total')]));
        $this->line(vsprintf('     pacote: <fg=green>%s</>', [Arr::get($results, 'totalizador.proximo_receber_pacote_total')]));
        $proximoLucro = Arr::get($results, 'totalizador.proximo_lucro');
        $this->line(vsprintf('   lucro: <fg=magenta>%s (%s)</>', [$proximoLucro, $proximoLucro * $dolar]));

        if ($lucro < 0) {
            $dias = ceil(abs($lucro) / $proximoLucro); //arredonda pra cima
            $this->line(vsprintf('   previsão zerar: <fg=magenta>%s dias</>', [$dias]));
        }

        $this->line(vsprintf('<fg=blue>proxima ordem a ser paga: %s</>', [$proximaOrdemId]));
    }

}
