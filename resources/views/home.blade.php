<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Laravel
                </div>

                <div class="links">
                    <a href="https://laravel.com/docs">Docs</a>
                    <a href="https://laracasts.com">Laracasts</a>
                    <a href="https://laravel-news.com">News</a>
                    <a href="https://blog.laravel.com">Blog</a>
                    <a href="https://nova.laravel.com">Nova</a>
                    <a href="https://forge.laravel.com">Forge</a>
                    <a href="https://github.com/laravel/laravel">GitHub</a>
                </div>
            </div>
        </div>




    <main>

{{--        $count = 0;--}}
{{--        foreach ($results['items'] as $item) {--}}
{{--        $count++;--}}

{{--        $this->line(vsprintf('<fg=yellow>%s - username: %s</>', [$count, Arr::get($item, 'authenticate.username')]));--}}

{{--        $this->line(' <fg=green>enviadas</>');--}}
{{--    $this->line('   valor total: <fg=green>' . Arr::get($item, 'enviado.valor_total') . '</>');--}}
{{--$this->line('   quantidade total: <fg=green>' . Arr::get($item, 'enviado.quantidade_total') . '</>');--}}

{{--$this->line(' <fg=green>recebidas</>');--}}
{{--$this->line(vsprintf('   valor total: <fg=green>%s</>', [Arr::get($item, 'recebido.valor_total'),]));--}}
{{--$this->line(vsprintf('     bonus: <fg=green>%s</>', [Arr::get($item, 'recebido.bonus_total')]));--}}
{{--$this->line(vsprintf('     pacote: <fg=green>%s</>', [Arr::get($item, 'recebido.pacote_total')]));--}}
{{--$this->line(vsprintf('   valor medio: <fg=green>%s</>', [Arr::get($item, 'recebido.valor_medio'),]));--}}
{{--$this->line('   quantidade total: <fg=green>' . Arr::get($item, 'recebido.quantidade_total') . '</>');--}}
{{--$this->line(' <fg=green>lucro</>');--}}
{{--$this->line('   valor: <fg=green>' . Arr::get($item, 'lucro') . '</>');--}}

{{--$this->line(' <fg=green>próximo</>');--}}
{{--$this->line('   receber: <fg=green>' . Arr::get($item, 'proximo.receber_total') . '</>');--}}
{{--$this->line('');--}}
{{--$this->line('');--}}
{{--}--}}

{{--$dolar = 4;--}}

{{--$this->line('<fg=blue>valor total</>');--}}
{{--$this->line('   enviadas: <fg=green>' . Arr::get($results, 'totalizador.enviado_valor_total') . '</>');--}}
{{--$this->line('   recebidas: <fg=green>' . Arr::get($results, 'totalizador.recebido_valor_total') . '</>');--}}
{{--$this->line('     bonus: <fg=green>' . Arr::get($results, 'totalizador.recebido_bonus_total') . '</>');--}}
{{--$this->line('     pacote: <fg=green>' . Arr::get($results, 'totalizador.recebido_pacote_total') . '</>');--}}
{{--$lucro = Arr::get($results, 'totalizador.lucro');--}}
{{--$this->line(vsprintf('   lucro: <fg=magenta>%s (%s)</>', [$lucro, $lucro * $dolar]));--}}
{{--$this->line('');--}}


{{--$this->line('<fg=blue>próximo</>');--}}
{{--$proximoEnviar = Arr::get($results, 'totalizador.proximo_enviar_total');--}}
{{--$this->line(vsprintf('   enviar: <fg=green>%s (%s)</>', [$proximoEnviar, $proximoEnviar * $dolar]));--}}
{{--$proximoReceber = Arr::get($results, 'totalizador.proximo_receber_total');--}}
{{--$this->line(vsprintf('   receber: <fg=green>%s (%s)</>', [$proximoReceber, $proximoReceber * $dolar]));--}}
{{--$this->line(vsprintf('     bonus: <fg=green>%s</>', [Arr::get($results, 'totalizador.proximo_receber_bonus_total')]));--}}
{{--$this->line(vsprintf('     pacote: <fg=green>%s</>', [Arr::get($results, 'totalizador.proximo_receber_pacote_total')]));--}}
{{--$proximoLucro = Arr::get($results, 'totalizador.proximo_lucro');--}}
{{--$this->line(vsprintf('   lucro: <fg=magenta>%s (%s)</>', [$proximoLucro, $proximoLucro * $dolar]));--}}

{{--if ($lucro < 0) {--}}
{{--$dias = ceil(abs($lucro) / $proximoLucro); //arredonda pra cima--}}
{{--$this->line(vsprintf('   previsão zerar: <fg=magenta>%s dias</>', [$dias]));--}}
{{--}--}}
    </main>

    </body>
</html>
